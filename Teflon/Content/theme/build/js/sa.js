﻿////    Main Namespace = SA
var sa = {};

////    This object is responsible to provide constants throughout the project
////    SA Constants
sa.constants = {
    mostViewedScreens: "/admin/GetMostViewedScreens",
    updateMostSelectedCoating: "/admin/GetMostSelectingCoatingType",
    login: "/admin/login",
    updateMostStayedScreens: "/admin/GetMostStayedScreen",
    addProducts: "/admin/AddOrEditProduct",
    uploadFile: "/admin/Upload",
    addFeatureProducts: "/admin/AddOrEditFeaturedProduct",
    changePassword: "/admin/ChangePassword",
    updateMostSearchProducts: "/admin/GetMostSearchProducts",
    updateFormula: "/admin/UpdateFormula",
    updateOperators: "/products/GetOperators",
    addBlog: "/admin/AddOrEditBlog",
    changeDateFormat: "/admin/ChangeDatetimeFormat",
    addSettings: "/Settings/AddOrEditSettings"
};

////    This object is responsible to provide utilities throughout the project
////    SA Utils
sa.utils = {
    color: function () {
        return '#' + (Math.random() * 0xFFFFFF << 0).toString(16);
    },
    upload: function (fileElement, callback) {
        NProgress.start();
        var formData = new FormData();
        formData.append('file', fileElement);
        $.ajax({
            url: sa.constants.uploadFile,
            type: 'POST',
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            error: function (error) {
                NProgress.done();
                callback(error);
            },
            success: function (data) {
                NProgress.done();
                if (data.Body === null) {
                    callback(data.Message);
                    return;
                }
                callback(null, data);
            }
        });
    }
};

////    This object is responsible to provide all authentication related stuff
////    Login Service
sa.loginService = {
    login: function() {
        NProgress.start();
        $.ajax({
            type: 'post',
            data: { Email: $("#email").val(), Password: $("#password").val()},
            url:sa.constants.login,
            success: function(res) {

                NProgress.done();
                if (res.Body === null) {
                    alert(res.Message);
                    return;
                }
                window.location.href = '/Home/Dashboard';
            }
        });
    }
};

////    This service is reponsible to provide data from dashboard
////    Dashboard Service
sa.dashboardService = {
    execute: function () {
        this.updateMostViewedScreens();
        this.updateMostSearchProducts();
        this.updateMostSelectedCoating();
        this.updateMostStayedScreens();
    },
    updateMostStayedScreens: function() {
        $.ajax({
            url:sa.constants.updateMostStayedScreens,
            success: function(res) {
                var resData = res.Body;
                if (resData === null) return;
                $("#most-stayed-screen-count").text(`(${resData.length})`);
                var countsArray = resData.map(object => object.Count);
                var highestValue = Math.max.apply(null, countsArray);
                resData.forEach((object, index) => {
                    var template = `<div><p>${resData[index].ProductName[0].toUpperCase() + resData[index].ProductName.substring(1).toLowerCase()}</p><div class=""><div class="progress progress_sm" style="width: 76%;"><div class="progress-bar bg-green" id="progress_${index}" role="progressbar" data-transitiongoal="${Math.round((object.Count / highestValue) * 100)}"></div></div></div></div>`;
                    $("#most-stayed-screen-graph-container").append(template);
                });
                resData.forEach((object, index) => {
                    var pro = Math.round((object.Count / highestValue) * 100);
                       $(`#progress_${index}`)
                            .attr("aria-valuenow", pro)
                            .animate({"width": pro + "%"});
                });
            }
        });
    },
    updateMostSelectedCoating: function () {
        $.ajax({
            url: sa.constants.updateMostSelectedCoating,
            success: function(res) {
                if (res.Body === null) return;

                $("#most-calculated-coating-count").text(`(${res.Body.length})`);


                var data = [], labels = [];
                res.Body.forEach((object, index) => {
                    data.push(object.Count);
                    labels.push(object.ProductName[0].toUpperCase() + object.ProductName.substring(1).toLowerCase());
                });

                var ctx = document.getElementById("pieChart");
                  var data1 = {
                    datasets: [{
                      data: data,
                      backgroundColor: [
                        "#455C73",
                        "#9B59B6",
                        "#BDC3C7",
                        "#26B99A",
                        "#3498DB"
                      ],
                      label: 'Coatings'
                    }],
                    labels: labels
                  };

                  var pieChart = new Chart(ctx, {
                    data: data1,
                    type: 'pie',
                    otpions: {
                      legend: false
                    }
                  });
            }
        });
    },
    updateMostSearchProducts: function(){
        $.ajax({
            url: sa.constants.updateMostSearchProducts,
            success: function(res) {
                if (res.Body === null) return;

                $("#most-searched-products-count").text(`(${res.Body.length})`);

               Morris.Bar({
                  element: 'most-searched-products',
                  data: res.Body,
                  xkey: 'ProductName',
                  ykeys: ['Count'],
                  labels: ['Count'],
                  barRatio: 0.4,
                  barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                  xLabelAngle: 35,
                  hideHover: 'auto',
                  resize: true
                });
            }
        });
    },
    updateMostViewedScreens: function () {
        $.ajax({
            url: sa.constants.mostViewedScreens,
            success: function (res) {

                $("#most-viewed-screens-count").text(`(${res.Body.length})`);
                if ($('.canvasDoughnut').length) {
                    var labels = [], bgColors = [
                            "#BDC3C7",
                            "#9B59B6",
                            "#E74C3C",
                            "#26B99A",
                            "#3498DB"
                        ], data = [], hovers =  [
                            "#CFD4D8",
                            "#B370CF",
                            "#E95E4F",
                            "#36CAAB",
                            "#49A9EA"
                        ];

                    var tableContainer = $(".view-screen-pie-chart");
                    $.each(res.Body, function(i, d){
                        d.ScreenName = d.ScreenName[0].toUpperCase() + d.ScreenName.substring(1).toLowerCase();
                        labels.push(d.ScreenName);
                        data.push(d.Count);

                        var doughnutTabular = `<tr><td><p><i class='fa fa-square' style='color:${bgColors[i]}'></i>${d.ScreenName}</p></td><td>${d.Count}</td></tr>`;
                        tableContainer.append(doughnutTabular);
                    });

                    var chart_doughnut_settings = {
                        type: 'doughnut',
                        tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                        data: {
                            labels: labels,
                            datasets: [{
                                data: data,
                                backgroundColor: bgColors,
                                hoverBackgroundColor: hovers
                            }]
                        },
                        options: {
                            legend: false,
                            responsive: false
                        }
                    }

                    $('.canvasDoughnut').each(function () {
                        var chart_element = $(this);
                        var chart_doughnut = new Chart(chart_element, chart_doughnut_settings);
                    });
                }
            }
        });
    }   
};

sa.changePassword = {
    update: function() {
        var data = {
            OldPassword: $(".Old-Password").val(),
            NewPassword: $(".New-Password").val(),
            ConfirmPassword: $(".Confirm-Password").val()

        };
        $.ajax({
            type: 'post',
            data: data,
            url: sa.constants.changePassword,
            success: function(res) {
                if (res.Status !== "Success") {
                    alert(res.Message);
                    return;
                }
                window.location.href = '/';
            }
        });
    }
};



sa.productService = {

    addProduct: function () {

        var data = {
            ProductID: $(".ProductID").val(),
            Code: $(".Code").val(),
            Type: $(".type").val(),
            Description: $(".Description").val(),
            Weight: $(".weight").val(),
            MaxUseTemp: $(".temp").val(),
            Color: $(".color").val(),
            Carrier: $(".carrier").val(),
            FilmBuildPerCoat: $(".Film").val(),
            MaxTotalFilmBuild: $(".MaxTotalFilm").val(),
            CureTemperature: $(".CureTemperature").val(),
            Coverage: $(".Coverage").val(),
            Thickness: $(".Thickness").val(),
            PrimaryProperties: $(".PrimaryProperties option:selected").text().replace("Stick", "Stick,").replace("Friction", "Friction,").replace("Heat Resistance", "Heat Resistance,").replace("Stability", "Stability,").replace("Chemical Resistance", "Chemical Resistance,").replace("Abrasion Resistance", "Abrasion Resistance,").replace("Strength", "Strength,").replace("Determined by Topcoat","Determined by Topcoat,"),
            ProductNotes: $(".ProductNotes").val(),
            FDA: document.getElementsByClassName('FDA')[0].checked,
            IsAvailableForCoating: document.getElementsByClassName('IsAvailableForCoating')[0].checked,
            IsActive: document.getElementsByClassName('Active')[0].checked,
            Type_es: $(".Type_es").val(),
            Description_es: $(".Description_es").val(),
            Color_es: $(".Color_es").val(),
            Carrier_es: $(".Carrier_es").val(),
            PrimaryProperties_es: $(".PrimaryProperties_es option:selected").text().replace("Antiadherente", "Antiadherente,").replace("fricción", "fricción,").replace("calor", "calor,").replace("criogénica", "criogénica,").replace("química", "química,").replace("abrasión", "abrasión,").replace("dieléctrica", "dieléctrica,").replace("Determinado por Topcoat","Determinado por Topcoat,"),
            ProductNotes_es: $(".ProductNotes_es").val()
        };
        $.ajax({
            type: 'post',
            data: data,
            url:sa.constants.addProducts,
            success: function(res) {
                if (res.Status !== "Success") {
                    alert(res.Message);
                    return;
                }
                window.location.href = '/home/product';
            }
        });
    },

    editProduct: function (ProductID) {

        this.ProductList.forEach((o, i) => {
            if (o.ProductID === ProductID)
            {
                var result = {};
                result.Body = o;
                $(".ProductID").val(result.Body.ProductID);
                $(".Code").val(result.Body.Code);
                $(".type").val(result.Body.Type);
                $(".Description").val(result.Body.Description);
                $(".weight").val(result.Body.Weight);
                $(".temp").val(result.Body.MaxUseTemp);
                $(".color").val(result.Body.Color);
                $(".carrier").val(result.Body.Carrier);
                $(".Film").val(result.Body.FilmBuildPerCoat);
                $(".MaxTotalFilm").val(result.Body.MaxTotalFilmBuild);
                $(".CureTemperature").val(result.Body.CureTemperature);
                $(".Coverage").val(result.Body.Coverage);
                $(".Thickness").val(result.Body.Thickness);

                //$('#example-multiple-selected').multiselect();
                //$('#example-multiple-selected2').multiselect();

                var data = result.Body.PrimaryProperties.split(',');
                for (var k = 0; k < data.length - 1; k++) {
                    $("#example-multiple-selected").find('option[value="' + data[k] + '"]').attr("selected", "selected");
                }
                var data2 = result.Body.PrimaryProperties_es.split(',');
                for (var j = 0; j < data2.length - 1; j++) {
                    $("#example-multiple-selected2").find('option[value="' + data2[j] + '"]').attr("selected", "selected");
                }

                $(".ProductNotes").val(result.Body.ProductNotes);

                $(".Active").attr("checked", result.Body.IsActive);
                $(".FDA").attr("checked", result.Body.FDA);
                $("#IsAvailableForCoating").prop("checked", result.Body.IsAvailableForCoating);
                $(".Type_es").val(result.Body.Type_es);
                $(".Description_es").val(result.Body.Description_es);
                $(".Color_es").val(result.Body.Color_es);
                $(".Carrier_es").val(result.Body.Carrier_es);
                $(".ProductNotes_es").val(result.Body.ProductNotes_es);
                window.scrollTo(0,0);
            }
        });
    }
};

sa.featuredProductService = {
    imageUrl: null,
    addFeatureProduct: function () {

        if (this.imageUrl === null) {
            alert("Image is required...");
            return;
        }

        var data = {
            FeaturedProductID: $(".ProductID").val(),
            Name: $(".productName").val(),
            ProductType: $(".productType").val(),
            Name_es: $(".productName_es").val(),
            ProductType_es: $(".productType_es").val(),
            ProductLink: $(".productLink").val(),
            ImageURI: this.imageUrl,
            IsActive: document.getElementsByClassName('Active')[0].checked
        };
        $.ajax({
            type: 'post',
            data: data,
            url: sa.constants.addFeatureProducts,
            success: function (res) {
                if (res.Status !== "Success") {
                    alert(res.Message);
                    return;
                }
                window.location.href = '/home/FeaturedProducts';
            }
        });
    },
    execute: function () {
        $("#featured-product-id").on('change', () => {
            var input = document.getElementById("featured-product-id");
            var url = $(input).val();
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0] && (ext === "gif" || ext === "png" || ext === "jpeg" || ext === "jpg")) {
                sa.utils.upload(input.files[0], (err, data) => {
                    //on upload completed
                    if (err) {
                        alert(err);
                        return;
                    }
                    this.imageUrl = data.Body;
                    var URI = this.imageUrl.replace('~', '');
                    $("#ProductImage").attr("src", URI);
                });
            } else {
                alert("Please select an image file");
            }
        });
    },
    editFeaturedProduct: function (ProductID, ProductImage) {

        if (ProductImage !== null) {
            ProductImage = ProductImage.replace('~', '');
            this.imageUrl = ProductImage;
            $("#ProductImage").attr("src", ProductImage);
        }

        this.FeaturedProductList.forEach((o, i) => {
            if (o.FeaturedProductID === ProductID) {
                $(".ProductID").val(o.FeaturedProductID);
                $(".productName").val(o.Name),
                $(".productType").val(o.ProductType),
                $(".productName_es").val(o.Name_es),
                $(".productType_es").val(o.ProductType_es),
                $(".productLink").val(o.ProductLink),
                $(".Active").attr('checked', o.IsActive),
                window.scrollTo(0, 0);
            }
        });
    }
};

sa.Blogs = {
    imageUrl: null,
    addBlog: function () {
        if (this.imageUrl === null) {
            alert("Image is required...");
            return;
        }

        var data = {
            BlogID: $(".blogID").val(),
            Title: $(".blogTitle").val(),
            Detail: $(".blogDetail").val(),
            Date: $(".blogDate").val(),
            State: $(".blogState option:selected").text(),
            Image: this.imageUrl,
            IsActive: document.getElementsByClassName('Active')[0].checked
        };

        $.ajax({
            type: 'post',
            data: data,
            url: sa.constants.addBlog,
            success: function (result)
            {
                if (result.Status !== "Success") {
                    alert(result.Message);
                    return;
                }
                window.location.href = '/home/Newsfeed';
            },
            error: function (err) {
                alert(err);
            }
        });
    },

    execute: function () {
        $("#news-blog-id").on('change', () => {
            var input = document.getElementById("news-blog-id");
            var url = $(input).val();
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0] && (ext === "gif" || ext === "png" || ext === "jpeg" || ext === "jpg")) {
                //var reader = new FileReader();
                //reader.onload = function (e) {
                sa.utils.upload(input.files[0], (err, data) => {
                    //on upload completed
                    if (err) {
                        alert(err);
                        return;
                    }
                    this.imageUrl = data.Body;
                    var URI = this.imageUrl.replace('~', '');
                    $("#BlogImage").attr("src", URI);
                });
                //};
            } else {
                alert("Please select an image file");
            }
        });
    },
    editBlog: function (BlogID, BlogDate, BlogImage) {

        //$('#StateDropdown').find('option[value=' + BlogState.replace(' ', '') + ']').prop('selected', true).trigger('change');

        if (BlogImage !== null) {
            BlogImage = BlogImage.replace('~', '');
            this.imageUrl = BlogImage;
            $("#BlogImage").attr("src", BlogImage);
        }

        var data = {
            Date: BlogDate
        };

        $.ajax({
            type: 'post',
            data: data,
            url: sa.constants.changeDateFormat,
            success: function(result) {
                $(".blogDate").val(result);
            }
        });

        for (var i = 0; i < this.BlogList.length; i++) {
            if (this.BlogList[i].BlogID === BlogID) {
                $(".blogID").val(this.BlogList[i].BlogID),
                    $(".blogTitle").val(this.BlogList[i].Title),
                    //$(".blogDetail").val(this.BlogList[i].Detail),
                    document.getElementsByClassName('Active')[0].checked = this.BlogList[i].IsActive,
                    window.scrollTo(0, 0);
            }
        }

        //this.BlogList.forEach((o, i) => {
        //    if (o.BlogID === BlogID) {
        //        $(".blogID").val(o.BlogID),
        //            $(".blogTitle").val(o.Title),
        //            $(".blogDetail").val(o.Detail),
        //            document.getElementsByClassName('Active')[0].checked = o.IsActive,
        //            window.scrollTo(0, 0);
        //    }
        //});
    }
};

sa.Formulas = {
    updateFormula: function ()
    {
        var data = {
            Operator1: $('.Operator1').val(),
            Operator2: $('.Operator2').val(),
            Operator3: $('.Operator3').val(),
            Operator4: $('.Operator4').val(),
        };

        $.ajax({
            type: 'post',
            data: data,
            url: sa.constants.updateFormula,
            success: function (result) {
                
            }
        });
    },

    getOperators: function ()
    {
        $.ajax({
            type: 'get',
            url: sa.constants.updateOperators,
            success: function (result) {
                result = JSON.parse(result);

                if (result.Operator1 === "Multiply")
                    document.getElementById("Operator1_Dropdown").selectedIndex = "0";
                else if (result.Operator1 === "Add")
                    document.getElementById("Operator1_Dropdown").selectedIndex = "2";
                else if (result.Operator1 === "Subtract")
                    document.getElementById("Operator1_Dropdown").selectedIndex = "3";
                else if (result.Operator1 === "Divide")
                    document.getElementById("Operator1_Dropdown").selectedIndex = "1";

                if (result.Operator2 === "Multiply")
                    document.getElementById("Operator2_Dropdown").selectedIndex = "0";
                else if (result.Operator2 === "Add")
                    document.getElementById("Operator2_Dropdown").selectedIndex = "2";
                else if (result.Operator2 === "Subtract")
                    document.getElementById("Operator2_Dropdown").selectedIndex = "3";
                else if (result.Operator2 === "Divide")
                    document.getElementById("Operator2_Dropdown").selectedIndex = "1";

                if (result.Operator3 === "Multiply")
                    document.getElementById("Operator3_Dropdown").selectedIndex = "0";
                else if (result.Operator3 === "Add")
                    document.getElementById("Operator3_Dropdown").selectedIndex = "2";
                else if (result.Operator3 === "Subtract")
                    document.getElementById("Operator3_Dropdown").selectedIndex = "3";
                else if (result.Operator3 === "Divide")
                    document.getElementById("Operator3_Dropdown").selectedIndex = "1";

                if (result.Operator4 === "Multiply")
                    document.getElementById("Operator4_Dropdown").selectedIndex = "0";
                else if (result.Operator4 === "Add")
                    document.getElementById("Operator4_Dropdown").selectedIndex = "2";
                else if (result.Operator4 === "Subtract")
                    document.getElementById("Operator4_Dropdown").selectedIndex = "3";
                else if (result.Operator4 === "Divide")
                    document.getElementById("Operator4_Dropdown").selectedIndex = "1";
            }
        });
    }
};

sa.SettingsService = {

    addSetting: function () {

        var list = [];
        
        list.push({
            SettingsID: $('.WebLinkID').val(),
            Content: $('.WebsiteLink').val(),
            Type: 'ContactWeb',
            CreatedDate: new Date().toISOString()
        });

        list.push({
            SettingsID: $('.NumberID').val(),
            Content: $('.PhoneNumber').val(),
            Type: 'ContactPhone',
            CreatedDate: new Date().toISOString()
        });

        list.push({
            SettingsID: $('.AboutUsID').val(),
            Content: $('.AboutUs').val(),
            Type: 'About-en',
            CreatedDate: new Date().toISOString()
        });

        list.push({
            SettingsID: $('.AboutUs_EsID').val(),
            Content: $('.AboutUs_Es').val(),
            Type: 'About-es',
            CreatedDate: new Date().toISOString()
        });

        $.ajax({
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify(list),
            url: sa.constants.addSettings,
            success: function (result) {

                window.location.href = '/home/Settings';
            }
        });
    }
};